speed = 30
sylphes = ['laline', 'blogane', 'thela', 'yzo', 'mayls', 'kawane']
nodes = [
    {
        'text': 'Bien le bonjour ! Bienvenue dans le monde magique des Poké-Sylphes ! ' +
            'Mon nom est Chen ! Les gens souvent m’appellent le Prof Poké-Sylphes! ' +
            'Ce monde est peuplé de créatures du nom de Sylphes !' +
            ' Pour certains, les Sylphes sont des compagnons de jeu, pour d’autres, ' +
            'ils sont un moyen de grandir avec les autres. ' +
            'Pour ma part… L’étude des Sylphes est ma profession !',
        'type': 'chen-paragraph',
        'ascendantUID': undefined,
        'descendentsUID': ['question1'],
        'UID': 'paragraph1',
    },
    {
        'text': 'Tout d\'abord, quel est ton prénom et ton nom ? ',
        'type': 'question',
        'ascendantUID': 'paragraph1',
        'descendentsUID': ['paragraph2'],
        'UID': 'question1',
    },
    {
        'text': 'OK! Tu t\'appelle donc {prenom} {nom} ! ' +
            'Puisque tu es ici {prenom}, je suppose que c’est pour m’aider. ' +
            'Vois-tu l’étude des Sylphes est très longue et je n’y arriverais pas tout seul. ' +
            'Je te propose de choisir deux Sylphes, ceux avec qui tu te sens en accord et avec qui tu voudrais évoluer. ' +
            'Fait bien attention à choisir des Sylphes que tu ne connais pas encore !',
        'type': 'chen-paragraph',
        'ascendantUID': 'question1',
        'descendentsUID': ['choice1'],
        'UID': 'paragraph2',
    },
    {
        'text': 'Vois tu ces poké-balls ? ' +
            'Chacune contient un poké-sylphe, tu vas pouvoir en apprendre un peu plus sur eux. ' +
            'Allez clique sur une poké-ball pour essayer !',
        'type': 'choice',
        'ascendantUID': 'paragraph2',
        'descendentsUID': ['Blogane','Laline', 'Thela', 'Yzo', 'Mayls', 'Kawane'],
        'UID': 'choice1',
    },
    {
        'text': 'Blogane le Sylphe solidaire ! Amicale, attentive et ouverte aux autres, ' +
            'Blogane aime accueillir les nouveaux venus, découvrir leurs passions et apporter le bonheur autour d’elle. ' +
            'Elle apprend autant de ses amis que de la nature et ses habitants.',
        'type': 'poke-paragraph',
        'ascendantUID': 'choice1',
        'descendentsUID': ['valid1'],
        'UID': 'blogane',
    },
    {
        'text': 'Laline le Sylphe dynamique ! Sportive, toujours le sourire aux lèvres, ' +
            'Laline est motivée que ce soit pour de nouvelles activités ou pour aider lors des services. ' +
            'Elle encourage les autres, contrôle sa force et est attentive à son hygiène de vie et à celle des autres.',
        'type': 'poke-paragraph',
        'ascendantUID': 'choice1',
        'descendentsUID': ['valid1'],
        'UID': 'laline',
    },
    {
        'text': 'Théla le Sylphe débrouillard ! ' +
            'Curieuse et observatrice, Théla aime découvrir, partager ses talents et ses idées. ' +
            'Habile de ses mains, elle expérimente et peut réaliser tout seule les taches qui lui sont confiées.',
        'type': 'poke-paragraph',
        'ascendantUID': 'choice1',
        'descendentsUID': ['valid1'],
        'UID': 'thela',
    },
    {
        'text': 'Yzô le Sylphe vrai ! Fiable et confiant, Yzô aime découvrir les autres ' +
            'en partageant avec eux ses secrets et sa confiance. ' +
            'Il est vrai, respecte les règles, fait ce qu’il dit ' +
            'et dit les choses en face mais toujours en faisant attention à ne pas blesser les autres. ',
        'type': 'poke-paragraph',
        'ascendantUID': 'choice1',
        'descendentsUID': ['valid1'],
        'UID': 'yzo',
    },
    {
        'text': 'Maÿls le Sylphe curieux de Dieu ! Attentif à l’autre et à lui-même, ' +
            'Maÿls vit en accord avec la loi des Louveteau/Janette et aime découvrir de nouveaux chemins. ' +
            'Il contemple la nature et réfléchis sur sa relation avec lui-même et avec les autres, ' +
            'il respecte tout le monde quelque-soit sa culture religieuse. ',
        'type': 'poke-paragraph',
        'ascendantUID': 'choice1',
        'descendentsUID': ['valid1'],
        'UID': 'mayls',
    },
    {
        'text': 'Kawane le Sylphe respectueux ! Joyeux et à l’écoute, ' +
            'Kawane prend soin de ses amis et est toujours là pour les aider. ' +
            'Il aime rencontrer de nouvelles personnes et les accueillir comme s’il les connaissait depuis longtemps. ' +
            'Toujours prêt à rire avec les autres, il déteste l’exclusion et la moquerie.',
        'type': 'poke-paragraph',
        'ascendantUID': 'choice1',
        'descendentsUID': ['valid1'],
        'UID': 'kawane',
    },
    {
        'text': 'Veux-tu choisir les deux poké-sylphes qui t\'intéressent ? ' +
            'Une fois ton choix confirmé tes chefs seront au mis au courant ' +
            'des poke-sylphe que tu souhaite le plus découvrir. ' +
            'Tu peux également cliquer sur une autre poké-ball !',
        'type': 'validation',
        'ascendantUID': 'choice1',
        'descendentsUID': ['confirm1'],
        'UID': 'valid1',
    },
    {
        'text': 'Tu as choisi {poke1} et {poke2}',
        'type': 'confirmation',
        'ascendantUID': 'choice1',
        'descendentsUID': undefined,
        'UID': 'confirm1',
    },
]

String.prototype.format = function() {
    let formatted = this;
    for (let i = 0; i < arguments.length; i++) {
        const regexp = new RegExp('\\{' + i + '\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};
String.prototype.formatUnicorn = String.prototype.formatUnicorn ||
    function () {
        "use strict";
        let str = this.toString();
        if (arguments.length) {
            const t = typeof arguments[0];
            let key;
            const args = ("string" === t || "number" === t) ?
                Array.prototype.slice.call(arguments)
                : arguments[0];

            for (key in args) {
                str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
            }
        }

        return str;
    };

class DialogueNode {
    constructor(text, type, ascendantUID, descendentsUID) {
        this.text = text;
        this.type = type;
        this.ascendantUID = ascendantUID;
        this.descendentsUID = descendentsUID;
    }
    fill_text(text_variables) {
        return this.text.formatUnicorn(text_variables);
    }
}
class Dialogue {
    constructor(currentNode, UID_to_node, environment) {
        this.curentNode = currentNode;
        this.UID_to_node = UID_to_node;
        this.env = environment;
    }
    get_previous_choice() {
        return this.curentNode.ascendantUID;

    }
    get_next_choices() {
        return this.curentNode.descendentsUID;
    }
    update_env(new_env_parameters, new_UID) {
        this.curentNode = this.UID_to_node[new_UID];
        for (let key in new_env_parameters) {
            this.env[key] = new_env_parameters[key];
        }
        return this.curentNode;
    }
    generate_text_from_env() {
        return this.curentNode.fill_text(this.env);
    }
}

function create_dialogue(node_list) {
    let UID_to_node = {}
    for (let i = 0; i < node_list.length; i++) {
        let _node = node_list[i];
        UID_to_node[_node['UID']] = new DialogueNode(_node['text'], _node['type'],
            _node['ascendantUID'], _node['descendentsUID']);
    }
    return new Dialogue(UID_to_node['paragraph1'], UID_to_node, {})
}


let dialogue = create_dialogue(nodes)
function setNavButton(typeNav) {
    let appText = document.querySelector('.app-text');
    let navButton = document.createElement('div');
    navButton.setAttribute('class', 'nav-button');
    let bPrev = document.createElement('button');
    let bNext = document.createElement('button');
    bPrev.setAttribute('value',
        (dialogue.curentNode.ascendantUID === undefined) ?
            'null' : dialogue.curentNode.ascendantUID);
    bPrev.setAttribute('class',
        (dialogue.curentNode.ascendantUID === undefined) ?
            'unValidButton' : 'validButton');
    bPrev.setAttribute('id', 'prevButton');

    bPrev.textContent = '<< Précédent';

    bNext.setAttribute('value', (dialogue.curentNode.descendentsUID === undefined) ?
        'null' : dialogue.curentNode.descendentsUID[0]);
    bNext.setAttribute('class',
        (dialogue.curentNode.descendentsUID === undefined) ?
            'unValidButton' : 'validButton');
    bNext.setAttribute('id', 'nextButton');

    bNext.textContent = 'Suivant >>';

    if(typeNav === 'chen-paragraph' || typeNav === 'poke-paragraph') {
        navButton.appendChild(bPrev);
        navButton.appendChild(bNext);
    } else if (typeNav === 'choice') {
        navButton.appendChild(bPrev);
    } else if (typeNav === 'question') {
        let prenom = document.createElement('input');
        let nom = document.createElement('input');
        prenom.setAttribute('placeholder', 'Prénom');
        prenom.setAttribute('type', 'text');
        prenom.setAttribute('id', 'inputPrenom');

        nom.setAttribute('placeholder', 'nom');
        nom.setAttribute('type', 'text');
        nom.setAttribute('id', 'inputNom');

        let bValid = document.createElement('button');

        bValid.setAttribute('id', 'nameForm');
        bValid.setAttribute('class', 'validButton');
        bValid.setAttribute('value', dialogue.curentNode.descendentsUID[0]);

        bValid.textContent = 'Envoyer!';
        navButton.appendChild(bPrev);
        navButton.appendChild(prenom);
        navButton.appendChild(nom);
        navButton.appendChild(bValid);
    } else if (typeNav === 'validation') {
        console.log('test validation');
        let bValid = document.createElement('button');
        let selectSylphe1 = document.createElement('select');
        let selectSylphe2 = document.createElement('select');

        selectSylphe1.setAttribute('name', 'sylphe1');
        selectSylphe1.setAttribute('id', 'sylphe1');

        selectSylphe2.setAttribute('name', 'sylphe2');
        selectSylphe2.setAttribute('id', 'sylphe2');
        for (let i = 0; i < sylphes.length; ++i) {
            let newOption1 = document.createElement('option');
            let newOption2 = document.createElement('option');

            newOption1.innerText = sylphes[i];
            newOption2.innerText = sylphes[i];

            newOption1.value = sylphes[i];
            newOption2.value = sylphes[i];

            selectSylphe1.appendChild(newOption1);
            selectSylphe2.appendChild(newOption2);
        }


        bValid.setAttribute('id', 'validPoke');
        bValid.setAttribute('class', 'validButton');
        bValid.setAttribute('value', 'confirm1');

        bValid.textContent = 'Valider !';
        navButton.appendChild(bPrev);
        navButton.appendChild(selectSylphe1);
        navButton.appendChild(selectSylphe2);
        navButton.appendChild(bValid);
    }
    appText.appendChild(navButton);
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
async function set_paragraph() {
    let appTex = document.querySelector('.app-text');
    appTex.innerHTML = '';
    let paragraphElement = document.createElement('p');
    paragraphElement.setAttribute('class', 'typewriter');
    appTex.appendChild(paragraphElement);
    let text = dialogue.generate_text_from_env();
    for (let i = 0; i < text.length; ++i) {
        paragraphElement.innerHTML += text.charAt(i);
        await sleep(speed);
    }
    setNavButton(dialogue.curentNode.type);

    paragraphElement.textContent = dialogue.generate_text_from_env();
}
const appText = document.querySelector('.app-text');
const appHeader = document.querySelector('.app-header');

function setCanvas(name) {
    let appCanvas = document.querySelector('.app-canvas');
    appCanvas.innerHTML = '';
    let newImg = document.createElement('img');
    newImg.src = 'img/' + name +'.png'
    newImg.alt = name;
    appCanvas.appendChild(newImg);
}

function nextAppText(e) {
    if (e.target && e.target.nodeName === "BUTTON" && e.target.value !== 'null') {
        if (e.target.id  === 'prevButton'  || e.target.id  === 'nextButton') {
            dialogue.update_env({}, e.target.value)
        } else if (e.target.id  === 'nameForm') {
            let inputPrenom = document.getElementById('inputPrenom');
            let inputNom = document.getElementById('inputNom');

            let prenomValue = inputPrenom.value;
            let nomValue = inputNom.value;
            if(prenomValue !== '' && nomValue !== '') {
                dialogue.update_env(
                    {'prenom': prenomValue, 'nom': nomValue},
                    e.target.value)
            }
        } else if (e.target.id  === 'validPoke') {
            let inputSylphe1 = document.getElementById('sylphe1');
            let inputSylphe2 = document.getElementById('sylphe2');
            let nameSylphe1 = inputSylphe1.value;
            let nameSylphe2 = inputSylphe2.value;
            dialogue.update_env({'poke1': nameSylphe1, 'poke2': nameSylphe2},
                e.target.value);
            setCanvas('prof-chen');
        }
        set_paragraph();
        let appHeader = document.querySelector('.app-header');
        appHeader.hidden = !(dialogue.curentNode.type === 'choice' ||
            dialogue.curentNode.type === 'poke-paragraph' ||
            dialogue.curentNode.type === 'validation');
        if (dialogue.curentNode.type === 'choice' ||
            dialogue.curentNode.type === 'paragraph' ) {
            setCanvas('prof-chen')
        }
    }
}
function nextAppHeader(e) {
    if (e.target && e.target.nodeName === "BUTTON" && e.target.value !== undefined) {
        dialogue.update_env({'pokesylphe':  e.target.value}, e.target.value);
    }
    set_paragraph();
    setCanvas(e.target.value);
}
function ready() {
    set_paragraph();
    appText.addEventListener('click', nextAppText);
    appHeader.addEventListener('click', nextAppHeader)
}
document.addEventListener("DOMContentLoaded", ready);
